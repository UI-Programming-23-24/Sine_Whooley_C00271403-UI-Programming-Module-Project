///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Setting up canvas
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

// Canvas Edges
canvas.width = window.innerWidth * 0.8;     // 80% of Window width
canvas.height = window.innerHeight * 0.9;   // 80% of Window height
canvas.top = 0;
canvas.left = 0;

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


//Variables
const scale = 0.5;

const width = 64;
const height = 72;
const mediumScaledWidth = scale * width;    // Scaled Width
const mediumScaledHeight = scale * height;  // Scaled Height

const smallWidth = 16;
const smallHeight = 18;
const smallScaledWidth = scale * smallWidth;    // Scaled Width
const smallScaledHeight = scale * smallHeight;  // Scaled Height

const largeWidth = 144;
const largeHeight = 128;
const largeScaledWidth = scale * largeWidth;    // Scaled Width
const largeScaledHeight = scale * largeHeight;  // Scaled Height

const splashWidth = 575;
const splashHeight = 328;
const splashScaledWidth = canvas.width;
const splashScaledHeight = canvas.height;

// Player 
const playerWalkLoop = [0, 1, 2, 3];
let playerDirection = 0;
let playerWalkLoopIndex = 0;

let playerWidth = width;
let playerHeight = height;
let playerSpeed = 5;
let playerFrameCount = 0;
let playerFrameSpeed = 7;

//Level 1
// Dino
let dinoSpeed = 1;

// Egg
let randomPosX = (Math.floor(Math.random() * canvas.width)- width);
let randomPosY = (Math.floor(Math.random() * canvas.height) - height);


// ScoreCounter
const scoreCounter = canvas.getContext('2d');
let score = 0;
let scoreIncrease = 2;
let scoreDecrease = 1;

let scWidth = 50;
let scHeight = 20;
let scMaxCount = 20;

// Lives Counter
const lifeCounter = canvas.getContext('2d');

let lives = 3
let lcWidth = 120;
let lcHeight = 30;
let lcMax = 120;
let lcTotal = 120;
let lcDecrease = 40;

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Images - Spritesheets
// Splash 
let splashImg = new Image();
splashImg.src = "assets/img/Opening-Splash/Opening-Splash1.png"

// Players 
// Boy
let playerAImg = new Image();
playerAImg.src = "assets/img/Player/playerA.png";
// Girl
let playerBImg = new Image();
playerBImg.src = "assets/img/Player/playerB.png";


// Level 1 - Dinosaurs
let volcanoImg = new Image();
volcanoImg.src = "assets/img/Dinos/Volcano.png";

let dinoImg = new Image();
dinoImg.src = "assets/img/Dinos/Dinosaur.png";

let nestImg = new Image();
nestImg.src = "assets/img/Dinos/Nest.png";

let eggImg = new Image();
eggImg.src = "assets/img/Dinos/Egg.png";

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Objects
// Creating Constructor - Holds positional information
function GameObject(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

// Creating Objects using constructor 
// Splash Screen Object 
let splash = new GameObject(splashImg, 0, 0, canvas.width, canvas.height);

// Player Object
let playerA = new GameObject(playerAImg, 0, 100, 200, 200); 
let playerB = new GameObject(playerBImg, 0, 100, 200, 200); 

// Level 1 - Dinosaurs - Objects
let volcano1 = new GameObject(volcanoImg, 0, 0, 200, 200);
let volcano2 = new GameObject(volcanoImg, 0, 0, 200, 200);
let volcano3 = new GameObject(volcanoImg, 0, 0, 200, 200);
let volcano4 = new GameObject(volcanoImg, 0, 0, 200, 200);
let volcano5 = new GameObject(volcanoImg, 0, 0, 200, 200);
let volcano6 = new GameObject(volcanoImg, 0, 0, 200, 200);

let nest = new GameObject(nestImg, 300, 500, 200, 200);
let egg = new GameObject(eggImg, randomPosX, randomPosY, 200, 200);

let dinoA = new GameObject(dinoImg, 100, 300, 200, 200);
let dinoB = new GameObject(dinoImg, 100, 300, 200, 200);
let dinoC = new GameObject(dinoImg, 100, 300, 200, 200);
let dinoD = new GameObject(dinoImg, 100, 300, 200, 200);


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Movement 
// Player
// Move Player Down
function movePlayerDown()
{
    playerB.y += playerSpeed;                    // Increase Players y-position  
    playerDirection = 0;                         // Sets Player Direction (Spritesheet Direction)

    // Wrap around screen
    if(playerB.y >= canvas.height)
    {
        playerB.y = canvas.top;                  // Sets new players.y
    }

    // Boundary Check
    if (playerB.y  >= canvas.height)
    {
        console.log("Player at Bottom Edge");   // Output to Console
        increaseScore();
    }

    // Calling functions to run
    checkCollisions();
}

// Move Player Up
function movePlayerUp()
{
    playerB.y -= playerSpeed;                   // Decrease Players y-position      
    playerDirection = 1;                        // Sets Player Direction (Spritesheet Direction)  

    // Wrap around screen
    if(playerB.y <= 0 - playerHeight)
    {
        playerB.y = canvas.height;              // Sets new players.y 
    }

    // Boundary Check
    if (playerB.y <= 0)
    {
        console.log("Player at Top Edge");    // Output to Console      
        increaseScore();
    }

    // Calling functions to run
    checkCollisions();
}

// Move Player Left 
function movePlayerLeft()
{
    playerB.x -= playerSpeed;                    // Increase Players x-position      
    playerDirection = 2;                        // Sets Player Direction (Spritesheet Direction)  

    // Wrap around screen
    if(playerB.x <= canvas.left - playerWidth)
    {
        playerB.x = canvas.width;                // Sets new players.x
    }
    
    // Boundary Check
    if (playerB.x <= canvas.left)
    {
        console.log("Player at Left Edge");    // Output to Console
        increaseScore();     
    }

    // Calling functions to run
    checkCollisions();
}

// Move Player Right
function movePlayerRight()
{
    playerB.x += playerSpeed;                    // Decrease Players x-position      
    playerDirection = 3;                        // Sets Player Direction (Spritesheet Direction)  

    // Wrap around screen
    if(playerB.x >= canvas.width)
    {
        playerB.x = 0 - (playerWidth/2);         // Sets new players.x
    }

    // Boundary Check
    if (playerB.x + width >= canvas.width)
    {
        console.log("Player at Right Edge");    // Output to Console 
        increaseScore();  
    }

    // Calling functions to run
    checkCollisions();
}

///////////////////////////////////////////////////////////////////////////

// Level 1    
// Enemy / Dinosaur movement
function moveDinos()
{
    // Calling functions to run
    moveDinoUp();
    moveDinoDown();
    moveDinoLeft();
    moveDinoRight();
}

// To move Dinosaur Down
function moveDinoDown()
{
    dinoA.y += dinoSpeed * 1.5;                 // Increases y by dinoSpeed * 1.5

    // Wrap around screen
    if(dinoA.y >= canvas.height + height*1.5)
    {
        dinoA.y = canvas.top + height/2;        // Sets new y position for DinoA
    }

    // Calling functions to run
    drawDinoA();
}

// To move Dinosaur Up
function moveDinoUp()
{
    dinoB.y -= dinoSpeed * 1.75;                // Increases y by dinoSpeed * 1.75

    // Wrap around screen
    if(dinoB.y <= canvas.top - height*2)
    {
        dinoB.y = canvas.height - height/2;     // Sets new y position for DinoB
    }

    // Calling functions to run 
    drawDinoB();
}

// To move Dinosaur Left
function moveDinoLeft()
{
    dinoC.x -= dinoSpeed * 1.25;                // Increases x by dinoSpeed * 1.25
    
    // Wrap around screen
    if(dinoC.x <= canvas.left - width*2)
    {
        dinoC.x = canvas.width - width;         // Sets new x position for DinoC
    }

    // Calling functions to run
    drawDinoC();
}

// To move Dinosaur Right
function moveDinoRight()
{
    dinoD.x += dinoSpeed;                       // Increases x by dinoSpeed 

    // Wrap around screen
    if(dinoD.x >= canvas.width)
    {
        dinoD.x = canvas.left - (width/2);      // Sets new x position for DinoD
    }

    // Calling functions to run
    drawDinoD();
}

///////////////////////////////////////////////////////////////////////////

// Egg 
// Set new random positions for x & y
function randomPos(rangeX, rangeY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);    // Return-(absolute value( the greatest integer(random number) * rangeX) - delta))
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);    // Return-(absolute value( the greatest integer(random number) * rangeY) - delta))
}

function newEgg()
{
    spawnNewEgg = new randomPos(canvas.width, canvas.height, 25);
}
// spawnNewEgg = new randomPos(canvas.width, canvas.height, 25);

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//User input
// Holds input from user/player - down, left, right, up, mouse-clicks
function PlayerInput(keyInput)
{
    this.action = keyInput;      // Holds current input as a string 
}

// User input auto set to 'None'
let playerInput = new PlayerInput("None");1

// Takes input from User 
function userInput(keyEvent)
{
    console.log(keyEvent);      // Outputs to Console

    if(keyEvent.type === "keyup")
    {
        playerDirection = 0;    // Sets direction of player to 0
    }

    if(keyEvent.type === "keydown")
    {
        switch(keyEvent.key)
        {
            case "w":
                playerInput = new PlayerInput("w");
                break;
            case "a":
                playerInput = new PlayerInput("a");
                break;
            case "s":
                playerInput = new PlayerInput("s");
                break;
            case "d":
                playerInput = new PlayerInput("d");
                break;
            case "Shift":
                playerInput = new PlayerInput("Shift");
                break;
            default:
                playerInput = new PlayerInput("None");
                break;
        }
    } 
    else
    {
        playerInput = new PlayerInput("None");
    }   
}

// Checks input from user and assigns an action
function updatePlayerInput()
{
    if(playerInput.action == "w")
    {
        movePlayerUp();                         // Calls function to move Player Up
    }
    else if(playerInput.action == "a")
    {
        movePlayerLeft();                       // Calls function to move Player Left
    }
    else if (playerInput.action === "s")
    {
        movePlayerDown();                       // Calls function to move Player Down
    }
    else if (playerInput.action === "d")
    {
        movePlayerRight();                      // Calls function to move Player Right
    }    
    else if (playerInput.action === "Shift")
    {
        drawSplash();                           // Calls functions to redraw splash
        splashScreen.style.display = "none";
    }

}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Blueprints
// For 16px x 18px
function drawSmallFrame(image, xFrame, yFrame, xCanvas, yCanvas)
{
    ctx.drawImage(image,
                xFrame * smallWidth,
                yFrame * smallHeight,
                smallWidth,
                smallHeight,
                xCanvas,
                yCanvas,
                smallScaledWidth,
                smallScaledHeight);
}

// For 64px x 72px
function drawMedFrame(image, xFrame, yFrame, xCanvas, yCanvas)
{
    ctx.drawImage(image,
                    xFrame * width,
                    yFrame * height,
                    width,
                    height,
                    xCanvas,
                    yCanvas,
                    mediumScaledWidth,
                    mediumScaledHeight);
}

// For 144px x 128px
function drawLargeFrame(image, xFrame, yFrame, xCanvas, yCanvas)
{
    ctx.drawImage(image,
                    xFrame * largeWidth,
                    yFrame * largeHeight,
                    largeWidth,
                    largeHeight,
                    xCanvas,
                    yCanvas,
                    largeScaledWidth,
                    largeScaledHeight);
}

// For 575px x 328px
function drawSplashFrame(image, xFrame, yFrame, xCanvas, yCanvas)
{
    ctx.drawImage(image,
                    xFrame * splashWidth,
                    yFrame * splashHeight,
                    splashWidth, 
                    splashHeight,
                    xCanvas,
                    yCanvas,
                    splashScaledWidth,
                    splashScaledHeight);
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Animations
// Using Blueprint  
// Calling all to run 
function drawSprites()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Calling functions to run   
    checkCollPlayerNest();          
    checkCollPlayerVolcano();         
    drawLevel1();
    animatePlayer(); 

}


// Player
function animatePlayer()
{
    // console.log("Drawing Player Reached");
    if(playerInput.action != "None")                // If player presses a key, continue
    {
        playerFrameCount++;                         // Increments frame count
      
        if(playerFrameCount >= playerFrameSpeed)    // If frame count is it greater than or equal to the Speed of the player 
        {
            playerFrameCount = 0;                   // Sets frame count back to Zero
            playerWalkLoopIndex++;                  // Incrementing the loop index number
            
            if(playerWalkLoopIndex >= playerWalkLoop.length)    // If players index position is greater than or equal to the lenght of the loop 
            {
                playerWalkLoopIndex = 0;            // Sets loop index back to 0
            }
        }
    }
    else                                            
    {
        playerWalkLoopIndex = 0;                    // If not, set index position to 0
    }
    
    // Draw Player on canvas
    drawMedFrame(playerB.spritesheet, playerWalkLoop[playerWalkLoopIndex], playerDirection, playerB.x, playerB.y);
}

///////////////////////////////////////////////////////////////////////////

// Level 1 - Dinosaurs
// Animate Level 1
function drawLevel1()
{
    // console.log("drawLevel1 Reached");

    // Calling functions to run
    drawVolcanos();
    drawNest();
    drawEggs();
    moveDinos();
}

// Volcano
// Enemy - Static
function drawVolcanos()
{
    // Calling functions to run
    drawVolcano1();
    drawVolcano2();
    drawVolcano3();
    drawVolcano4();
    drawVolcano5();
    drawVolcano6();
}

function drawVolcano1()
{
    drawMedFrame(volcano1.spritesheet, 0, 0, canvas.width / 7, canvas.height / 3);
}      
function drawVolcano2()
{
    drawMedFrame(volcano2.spritesheet, 0, 0, canvas.width / 2, canvas.height / 9.7);
}
function drawVolcano3()
{ 
    drawMedFrame(volcano3.spritesheet, 0, 0, canvas.width - (canvas.width/ 6), canvas.height / 7);
}
function drawVolcano4()
{
    drawMedFrame(volcano4.spritesheet, 0, 0, canvas.width/2, canvas.height / 2.5);
}
function drawVolcano5()
{
    drawMedFrame(volcano5.spritesheet, 0, 0, canvas.width /3, canvas.height - 140);
}
function drawVolcano6()
{
    drawMedFrame(volcano6.spritesheet, 0, 0, canvas.width - (width* 2) , canvas.height - (canvas.height/ 3));
}


// Fuel Depo for Level 1 - Nest
function drawNest()
{
    drawLargeFrame(nest.spritesheet, 0, 0, canvas.width / 2, canvas.height - height);
}


// Enemy - Moving - Dinosaurs
function drawDinoA()
{    
    drawMedFrame(dinoA.spritesheet, 0, 0, dinoA.x + (canvas.width/2 + 50) , dinoA.y - 100);
}

function drawDinoB()
{
    drawMedFrame(dinoB.spritesheet, 0, 0, dinoB.x + 200, dinoB.y + 64);
}
function drawDinoC()
{
    drawMedFrame(dinoC.spritesheet, 0, 0, dinoC.x + 90, dinoC.y -50);
}
function drawDinoD()
{
    drawMedFrame(dinoD.spritesheet, 0, 0, dinoD.x, dinoD.y + 125);
}


// Collectable - Eggs
function drawEggs()
{
    drawSmallFrame(egg.spritesheet, 0,0, randomPosX, randomPosY);   
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Collision Dectection 

//if (rect1.x < rect2.x + rect2.width && 
//    rect1.x + rect1.width > rect2.x && 
//    rect1.y < rect2.y + rect2.height &&
//    rect1.y + rect1.height > rect2.y) 
// {
    //  collision detected!
// }

// Level 1
// Player - DinoA
function checkCollisions()
{   
    // Calling function to run 
    checkCollPlayerVolcano();     
    checkCollPlayerNest();
    checkCollPlayerEgg();
}

// Player - Fuel Depo - Nest
function checkCollPlayerNest()
{
    // Player - Nest
    if( nest.x < (playerB.x - width)  &&                 // Left to Right
        (nest.x + largeWidth) > playerB.x &&             // Right to Left
        nest.y  < (playerB.y + largeScaledHeight)  &&    // Top to Bottom
        (nest.y - smallHeight) > playerB.y )             // Bottom to Top
    {
        console.log("Collision detected!!!!! -- Player & Nest");
        decreaseScore();
    }

}
// Player - Volcano 
function checkCollPlayerVolcano()
{
    // Player - Volcano
    if( volcano1.x > (playerB.x - width/2)  &&         // Left to Right
        (volcano1.x + width/2) > playerB.x &&          // Right to Left
        volcano1.y  < (playerB.y + height/2)  &&       // Top to Bottom
        (volcano1.y - smallHeight*2) > playerB.y )     // Bottom to Top
    {
        console.log("Collision detected!!!!! -- Player & Volcano 1");
        decreaseScore();
    }
}


// Player - Egg
function checkCollPlayerEgg()
{
    // Egg & Player
    if(playerB.x < egg.x - width && 
        playerB.x + smallWidth > egg.x &&
        playerB.y < egg.y + smallScaledHeight &&
        playerB.y - smallHeight > egg.y)
    {
        console.log("Player collected Egg");

        newEgg();
        spawnNewEgg = new randomPos(canvas.width, canvas.height, 25);
        decreaseScore();
    }
    
    drawEggs();            // Calls drawEggs function to run during collision detection
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Audio Assets

// soundOnOff()
// {

// }


function splashOpenAudio()
{
    let splashAudio = new Audio("assets/audio/splash.mp3");
    console.log(splashAudio);

    function playSplashSound()
    {
        console.log("Calling Splash Sound");
        splashAudio.play();
    }
    playSplashSound();
}

function gamePlayAudio()
{
    let gameAudio = new Audio("assets/audio/GamePlay.mp3");
    console.log(gameAudio);

    function playGamePlaySound()
    {
        console.log("Calling Splash Sound");
        gameAudio.play();
    }
    playGamePlaySound();
}


function pickUpAudio()
{
    let pickUpAudio = new Audio("assets/audio/pick-up.mp3");    // Variable - new audio sound
    console.log(pickUpAudio);                   // Outputs Sound to Console

    // To play sound
    function pickUpSound()                      
    {
        console.log("Calling Pick-Up Sound");   // Output to Console
        pickUpAudio.play();                     // Plays Audio
    }
        pickUpSound();                          // Calls function to run 
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Splash
function drawSplash()
{
    drawSplashFrame(splash.spritesheet, 0, 0, splash.x, splash.y);
    splashScreen.style.display = "contents";
    introButton.style.display = "none";
    rulesHelp.style.display = "none";
    forms.style.display = "none";
    splashOpenAudio();           // Works if you uncomment :)
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Score counter 
// To draw Score Counter
function updateScoreCounter()
{
    scoreCounter.clearRect(0, (canvas.height + scHeight), canvas.width, canvas.height); // Clears Rectangle
    scoreCounter.font = "20px Bold Sans Italic";           // Setting font 

    // New Score displaying to screen  
    scoreCounter.save(); 
    scoreCounter.fillStyle = "black";           // Setting Color
    scoreCounter.fillText("Total: " + score, (canvas.left +  scWidth), (canvas.top + (2 * scHeight)));
    // scoreCounter.fillText(nameInput + "'s Score: " + score, (canvas.left +  scWidth), (canvas.top + (2 * scHeight)));
    scoreCounter.restore();
}

// To increase score 
function increaseScore()
{
    score += scoreIncrease; // Increases when boundary is hit
    console.log(score);
    updateScoreCounter();
}

// To decrease score
function decreaseScore()
{
    score -= scoreDecrease;
    console.log(score);
    updateScoreCounter();
}
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// Lives Counter
function drawLivesCounter()
{
    // lifeCounter.clearRect(0, (canvas.height + height), canvas.width, canvas.height); // Clears Rectangle
    console.log("Life Counter Reached");

    let fillLives = Math.min(Math.max(lcTotal / lcMax, 0), 1);
    
    // Drawing the background bar
    lifeCounter.fillStyle = "white";
    lifeCounter.fillRect((canvas.left + (lcWidth/2)), (canvas.top + (3 * lcHeight)), lcWidth, lcHeight);
        
    // Drawing the regression bar
    lifeCounter.fillStyle = "grey";
    lifeCounter.fillRect((canvas.left + (lcWidth/2)), (canvas.top + (3 * lcHeight)), fillLives * lcWidth, lcHeight);
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


//Game Loop 
function gameLoop()
{
    drawSprites();
    updatePlayerInput();
    updateScoreCounter();

    window.requestAnimationFrame(gameLoop); // Animates all in loop
}

// gameLoop(); // Calls function to run


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Help 
// Controls menu for User
function controlInfo()
{
    rulesHelp.style.display = "contents";       // Displays Rules paragraph
    canvas.style.display = "none";              // Hides Canvas 
    splashScreen.style.display = "none";        // Hides Splash Screen
    helpButtons.style.display = "contents";     // Displays Buttons
    forms.style.display = "none";
}

// controlInfo();

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Exit Game
function exitGame()
{
    console.log("exitGame reached");

    canvas.style.display = "none";              // Hides canvas
    splashScreen.style.display = "none";        // Hides Splash Screen

    exitText();
}

function exitText()     // Does not display 
{
    console.log("exitText reached");
    const exitTextExit = canvas.getContext('2d');
    exitTextExit.font = "35px Arial";                                                       // Sets font
    exitTextExit.fillStyle = "Green";                                                       // Sets Colour
    exitTextExit.fillText("You have exit the game  Hit Refresh to start again",200,80);     // Sets text to display
}

function exit()
{
    console.log("exit reached");

    rulesHelp.style.display = "none";
    canvas.style.display = "contents";              // Hides canvas
    splashScreen.style.display = "contents";        // Hides Splash Screen
    helpButtons.style.display = "none";         // Hides Exit Button
    para.style.display = "none";                // Hides paragraph of text

    // drawSplash();
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

function playGame()
{
    console.log("playGame loop reached");
    
    introScreen.style.display = 'none';         // Hides intro Screen
    introButton.style.display = 'none';         // Hides intro Button
    splashScreen.style.display = "none";        // Hides Splash Screen

    canvas.style.outline = "black";             // Sets canvas outline colour to black
    canvas.style.backgroundColor = "lightgrey"; // Sets canvas background colour to grey 
    canvas.style.display = 'center';            // Show game canvas 
    gameLoop();
    gamePlayAudio();
}

// playGame();

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Event Listeners
window.addEventListener("keydown", userInput);
window.addEventListener("keyup", userInput);

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Forms
const entryForm = document.getElementById('forms');

if(typeof(Storage) !== "undefined")
{
    const userName = localStorage.getItem('userName');

    if(userName)
    {
        // let form = document.forms["userInput"];
        // form.style.display = "none";
        
        // let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        // let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        
        // validateButton.onclick = function(){
        //     window.location.href = "gameplay.html";
        // }

        // dismissButton.onclick = function(){
        //     welcomeScreen.style.display = "none";
        //     form.style.display = "block";
        // }
    }

    else
    {
        console.log("no data in localStorage, loading new session")
    }
}    
else 
{
    console.log(" Local storage is not supported ");
}


// User inputs their name 
function userNameForm()
{
    introScreen.style.display = "none";
    introButton.style.display = 'none';

    var nameInput = document.forms["userInput"]["userName"].value;
    
    if(nameInput == "")
    {
        alert("Please enter a Name");
        return false;
    }
    else
    {
        alert("Welcome to  " 
        + document.forms["userInput"]["userName"].value);


    }

    localStorage.setItem("userInput", nameInput);   
    introScreen.style.display = "contents";

    scoreCounter.save(); 
    scoreCounter.fillStyle = "black";           // Setting Color
    scoreCounter.fillText("Total: " + score, (canvas.left +  scWidth), (canvas.top + (2 * scHeight)));
    scoreCounter.restore();
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
